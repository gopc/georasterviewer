import numpy as np
import unittest

import geo_raster_viewer


def create_test_region():
    return geo_raster_viewer.GeoRasterViewer(
        pixel_array=np.ones((4, 6), dtype=np.float64),
        lon_min=-3.,
        lon_pixel_size=1.,
        lat_max=2.,
        lat_pixel_size=-1.,
    )


class TestGetPixelCentroids(unittest.TestCase):
    """Unit tests for _get_pixel_centroids()"""

    def test_centroids_correct(self):
        test_region = create_test_region()

        expected_lat_centroids = np.array([1.5, 0.5, -0.5, -1.5])
        expected_lon_centroids = np.array([-2.5, -1.5, -0.5, 0.5, 1.5, 2.5])
        expected_lats, expected_lons = np.meshgrid(expected_lat_centroids, expected_lon_centroids, indexing='ij')

        observed_lats, observed_lons = test_region._get_pixel_centroids()

        np.testing.assert_array_equal(expected_lats, observed_lats)
        np.testing.assert_array_equal(expected_lons, observed_lons)

    def test_correct_shape(self):
        """Tests that the grid has the correct shape."""
        test_region = create_test_region()

        observed_lats, observed_lons = test_region._get_pixel_centroids()

        self.assertEqual(observed_lats.shape, test_region.pixel_array.shape)
        self.assertEqual(observed_lons.shape, test_region.pixel_array.shape)


class TestMatchToClosestLocation(unittest.TestCase):
    """Unit tests for match_to_closest_location()"""

    def test_correct_shape(self):
        """Tests that the shape of the array returned by the method matches the shape of the pixel array."""
        test_region = create_test_region()

        test_locations = [
            (0.0, 0.0),
            (1.0, 1.0),
        ]

        closest_locations = test_region.match_to_closest_location(test_locations)

        self.assertEqual(test_region.pixel_array.shape, closest_locations.shape)

    def test_correct_answer(self):
        """Tests the answer is correct for a simple example."""
        test_region = create_test_region()

        test_locations = [
            (1.5, -1.5),
            (-1.0, 1.0),
        ]

        closest_locations = test_region.match_to_closest_location(test_locations)

        expected_labels = np.array(
            [
                [0, 0, 0, 0, 1, 1],
                [0, 0, 0, 1, 1, 1],
                [0, 0, 1, 1, 1, 1],
                [0, 1, 1, 1, 1, 1],
            ]
        )

        np.testing.assert_array_equal(closest_locations, expected_labels)
