import haversine
import math
import numpy as np
import random
import unittest

import haversine_vec


class TestComputeDistance(unittest.TestCase):
    """Unit tests for the compute_distance() method."""

    def test_same_as_external_library(self):
        """Tests that my method gives the same answer as the haversine library."""
        start_lat = random.uniform(-50, 50)
        start_lon = random.uniform(-180, 180)
        end_lat = random.uniform(-50, 50)
        end_lon = random.uniform(-180, 180)

        my_dist = haversine_vec.compute_distance(
            start_lat,
            start_lon,
            end_lat,
            end_lon
        )

        their_dist = haversine.haversine(
            (start_lat, start_lon),
            (end_lat, end_lon)
        )

        self.assertAlmostEqual(my_dist, their_dist)

    def test_same_shape_array_endpoint(self):
        """Tests shape of returned distances matches shape of start/end lat/lon arrays for an array of endpoints."""
        start_lat = np.random.random((2, 4))
        start_lon = np.random.random((2, 4))
        end_lat = np.random.random(6)
        end_lon = np.random.random(6)

        distances = haversine_vec.compute_distance(
            start_lat,
            start_lon,
            end_lat,
            end_lon
        )

        expected_shape = (end_lat.shape[0], start_lat.shape[0], start_lat.shape[1])
        self.assertEqual(expected_shape, distances.shape)

    def test_same_shape_scalar_endpoint(self):
        """Tests shape of returned distances matches shape of start/end lat/lon arrays for a scalar endpoint."""
        start_lat = np.random.random((2, 4))
        start_lon = np.random.random((2, 4))
        end_lat = random.random()
        end_lon = random.random()

        distances = haversine_vec.compute_distance(
            start_lat,
            start_lon,
            end_lat,
            end_lon
        )

        expected_shape = (1, start_lat.shape[0], start_lat.shape[1])
        self.assertEqual(expected_shape, distances.shape)

    def test_nans_not_allowed(self):
        """Tests that nan values in any of the inputs is not allowed."""
        start_lat = np.array([np.nan, 1.0])
        start_lon = np.array([np.nan, 1.0])
        end_lat = 0.
        end_lon = 0.

        with self.assertRaises(RuntimeError):
            haversine_vec.compute_distance(
                start_lat,
                start_lon,
                end_lat,
                end_lon
            )

        start_lat = np.array([1.0, 1.0])
        start_lon = np.array([1.0, 1.0])
        end_lat = math.nan
        end_lon = 0.

        with self.assertRaises(RuntimeError):
            haversine_vec.compute_distance(
                start_lat,
                start_lon,
                end_lat,
                end_lon
            )
